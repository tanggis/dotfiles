# Tanggis's dotfiles
This is trying to make recreating my environment easy across different machines.
Feel free to fork it and suggest pull requests.

### Dependencies
git: to pull vim plugins as git submodules

```
sudo apt install build-essential cmake python3-dev
```

### Installation

```bash
git clone git@bitbucket.org:tanggis/dotfiles.git && cd dotfiles && ./install.sh
```


### Contents
##### .vimrc and .vim/
* vim settings and plugins.
* plugins are currently managed by
 [pathogen](https://github.com/tpope/vim-pathogen), but will switch to
 [vim-plug](https://github.com/junegunn/vim-plug) when I got time...

##### .bashrc
##### .alias
##### .pymolrc
##### .hgrc
##### .hgignore
##### .gitconfig
##### .gitignore


### Credits
* `.vimrc .vim` adapted from
[sontek/dotfiles](https://github.com/sontek/dotfiles/tree/6a13c9b072ecf8dafa965b9412369ac956787dc7),
before he deleted his `.vim` and switched to emacs
